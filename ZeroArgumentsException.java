//exception thrown when a theoretically non-zero argument is zero
public class ZeroArgumentsException
extends		 Exception
{
	public ZeroArgumentsException(String name)
	{
		super("You tried to send 0 as an argument "+name+", while this programs only accepts strictly positive arguments...\nPlease try GameOfLife -h");
	}
}
