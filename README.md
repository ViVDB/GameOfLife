# GameOfLife

This is an implementation of the Game of Life from John Conway. It was a university project for the class "Object-oriented programming" (INFO0062) by Bernard Boigelot in 2018, at ULiège.

I designed it to have a lot of options, and more can be added. It shouldn't be too complicated to port this to a graphical user interface either. Using some of those files, it's also not too hard to set up a program where you would choose the game to play, each game using some commin settings.

You are free to use this to do whatever you want, within the limits of the licence, I invite people who want to learn java to try and make a graphical user interface by themselves, or to try and add options.

## COpyright

Copyright (C) notice for the code : 2018 ViVDB