
/*
This class is used for side cells that respect nothing so need more methods in order to work correctly.
Basically it adds the counter to the cell. In addition, a rightside cell has to jump line
I chose only 1 line jump because I found it prettier, especially with options : colour_dead black colour_alive invisible b_colour_dead white b_colour_alive carnival
*/
public class SideThug
extends 	 Cell
{
	//additionnal variables : 
	private	 final boolean left;//is true if sidethug is a left cell
	private int born;//this counts the number of cells born here
	private int dead;//this counts the number of cells buried here
	
	//Constructor
	/*
	col is the column of the cell
	proba is the probability for it to be born at initialization
	*/
	SideThug(boolean left,float proba)
	{
		super(proba);
		this.left=left;
		dead=0;
		if (isAlive())
			born=1;
		else
			born=0;
		
	}
	
	//methods
	
	//this shows the cell on screen
	public void showUp()
	{
		if (left)
		{
			showCount();
			super.showUp();
		}
		else
		{
			super.showUp();
			showCount();
			System.out.print("\n");
		}
		this.showNext();
	}
	
	
	//this show the counter part of the cell on screen
	private void showCount()
	{
		System.out.print(" [+ "+ String.format("%3d",born)+" /- "+String.format("%3d",dead)+"] ");
	}
	
	
	//this actually counts new and dead cells, in addition to checking of it should die. 
	public void testCell()
	{
		super.testCell();
		if (super.isBorn())
			born++;
		else if (super.isDead())
			dead++;
	}
}
