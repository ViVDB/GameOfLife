import java.io.IOException;

/*
	this class gives some useful methods that can be used anywhere
*/
public class Miscellaneous
{
	//this method removes the end of the array Tab of Cells to make it to the size newSize
	public static Cell[] removeEnd(Cell[] tab, int newSize)
	{
		Cell[] cp = new Cell[newSize];//Cp pour Copy
		
		for(int i=0;i<newSize;i++)
			cp[i]=tab[i];
		
		return cp;
	}
	
	//this method removes the first element of String array
	public static String[] removeFirst(String[] tab)
	{
		return removeFirst(tab, 1);
	}
	
	//this method method removes the first x elements of a String array
	//no defensive programmation since only this program will access this method, but defensive programmation can be implemented later
	public static String[] removeFirst(String[] tab, int x)
	{
		String[] cp  = new String[tab.length-x];
		for (int i=x;i<tab.length;i++)
			cp[i-x]=tab[i];
		return cp;
	}
	
	//this method lets the user read a message before going on with the program.
	public static void waiter()
	{
	    //in the future, settings should change this to either wait for input or wait some time
		System.out.println("\n\nPress the 'enter' key to go on\n");
		try
		{
			System.in.read();
		}
		catch(IOException e)
		{
			e.printStackTrace();//it's not critical, the game can go on even if the user didn't read everything
		}
		return;
	}
}
