
/*
This class is the main class and is used to sort out arguments
It also handles exception of argument formatting
*/
public class GameOfLife
{
	//this is the only method, obviously it does what the class does
	/*
		in short the arguments are : (for more info see HelpGeneral.txt and HelpOptions.txt)
		n m --> n is the size of the square, m is the size of the random square inside
		n --> n and m will be equal
		-h --> shows more info about arguments
		-s [option] [value] --> changes settings
		-h -s --> shows more info about settings
		-p play game
	*/
	public static void main(String[] args)
	{
		if (args.length==0)
		{
			args = new String[]{"default"};
			System.out.println("There was no argument, default settings apply.");//this is not an exception, merely a default behaviour
		}
		int n=0,m=0,i=0;
		switch (args[0].toLowerCase())
		{
			case "help":
			case "-h":
			case "--help":
			
				args=Miscellaneous.removeFirst(args);
				Help.show(args);
				break;
			
			case "-s":
			case "settings":
			case "--settings":
				
				args=Miscellaneous.removeFirst(args);
				
				SettingsSpecifics.main(args);

				break;
			
			//case "GUI"
			
			case "-d":
			case "default":
			case "--default":
				
				try
				{
					n=Integer.valueOf(SettingsSpecifics.getProp("n"));
					m=Integer.valueOf(SettingsSpecifics.getProp("m"));
					//there is no break, the main code comes later
				}
				catch(NoSuchPropException exc)
				{
					exc.printStackTrace();
					System.exit(-1);
				}
				
				
			case "-p":
			case "--play":
			case "play":
			
				i++;//this is because -P m n are the arguments
				
				
			default:
				if (m==0) //this if takes the arguments from input
				//Some conditions are checked only in SquareBoard
				{
					try
					{
						n=Integer.parseInt(args[i]);
						try
						{
							m=Integer.parseInt(args[i+1]);
							if(args.length>i+2)
								throw new TooManyArgumentsException(i+2);
								//for the sake of always knowing what's Exception Handling and what's not,
								//it's probably better not to do a simple println("")...
						}
						catch(IndexOutOfBoundsException exc)
						{
							System.err.println("\n\nPlease note that you used only one number, so n and m will be equal\n");
							
							Miscellaneous.waiter();
							
							m=n;
						}
						catch(TooManyArgumentsException exc)
						{
							System.err.println(exc.getMessage());//there are unused arguments but that doesn't hinder the game
							
							Miscellaneous.waiter();
							
						}
					}
					catch(Exception exc)
					{
						System.err.println("There is a problem in your arguments, please try GameOfLife -h for more informations...");
						System.err.println("Or maybe you just formatted your integer arguments in a weird way");
						System.err.printf("\n\n");
						System.err.println(exc.getMessage());
						System.exit(0);
					}
				}//end of if
				
				
				int Waiter=500;
				try
				{
					Waiter=Integer.valueOf(SettingsSpecifics.getProp("Waiter"));
				}
				catch(NoSuchPropException exc)
				{
					exc.printStackTrace();
					System.exit(-1);
				}
				
				
				
				Board b=new SquareBoard(m,n);//b for board
				
				
				while (b.getActivity())
				{
					b.show();
					b.update();
					try 
					{ 
						Thread.sleep(Waiter);
					} 
					catch(InterruptedException exc)
					{
						System.err.println(exc.getMessage());
					}
				}
				
				System.out.println("Thanks for playing this game!");
				//I know this never appear, but I hoped to find a way to use keyEvents in the console when I wrote this and it kills me to remove it
				
			
		}//end of switch

	}//end of main

}//end of GameOfLife
