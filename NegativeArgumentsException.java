//Exception thrown when a method receives negative arguments while it shouldn't
public class NegativeArgumentsException
extends		 Exception
{
	public NegativeArgumentsException(int m, String n)
	{
		super("You tried to send "+m+" as an argument "+n+", while this programs only accepts positive arguments...\nPlease try GameOfLife -h");
	}
}
