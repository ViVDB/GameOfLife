
/*
This Class is for a Board that has a square shape
Its responsibility is basically to initialize the board, as every cell has a pointer to the next one
It also handles common numerical exceptions
*/
public class SquareBoard
extends 	 Board
{


	//here is the only persistent variable
	/*
	private Cell[][] table;//this is the table of cells
	//first the columns, then the lines
	it disappears after initialization
	private final int N;//this is the length of a side of the square board
	this too will disappear
	*/
	private Cell first; //this is the first cell
	
	
	
	//here is the constructor
	/*
	m is the side of the random square of cells in the middle
	N is the side length
	0<m<=N and (N-m)%2==0
	no exception is thrown because everything is handled inside the method, the program stops if necessary
	*/
	public SquareBoard(int m,int n)
	{
		super();
		
		//Basic exception handling (some other exception handling is done later in the instanciation for convenience
		try
		{
			if (m>n)
				throw new InconsistentArgumentsException();
			if (n>Integer.valueOf(SettingsSpecifics.getProp("limit")))
				throw new BigArgumentsException(n,Integer.valueOf(SettingsSpecifics.getProp("limit")));
			if (m<0)
				throw new NegativeArgumentsException(m,"m");
			if (n<0)
				throw new NegativeArgumentsException(n,"n");
			if (m==0)
				throw new ZeroArgumentsException("m");
			if (n==0)
				throw new ZeroArgumentsException("n");
		}
		catch(NegativeArgumentsException exc)
		{
			System.err.println(exc.getMessage());
			System.exit(0);
		}
		catch(ZeroArgumentsException exc)
		{
			System.err.println(exc.getMessage());
			System.exit(0);
		}
		catch (InconsistentArgumentsException exc)
		{
			m=n;
			System.err.println("The size of the random square has been authomatically adjusted");
			
			Miscellaneous.waiter();
			
			System.err.println("\n\nThat would have been the default behavior anyways...\nMessage to the person who will mark this assignment :\nby default, this program should just say m=N and go on even though m>N, but since we are supposed to stop here, I will. Please try comenting out these lines too.");
			System.exit(0);
		}
		catch (BigArgumentsException exc)
		{
			System.err.println(exc.getMessage());
			System.exit(0);
		}
		catch (NoSuchPropException exc)
		{
			exc.printStackTrace();
			System.exit(-1);//-1 and not 0 because it's my mistake, not the user -- this is also why I don't catch Exception here
			
		}
		
		
		
		
		Cell[][] table = new Cell[n][n];//this is the table of cells
		//first the columns, then the lines
		//this is just to be populated, the reference will disappear later on because each cell points to the next one
		float proba=(float)0.3;
		try
		{
			proba =Float.valueOf(SettingsSpecifics.getProp("proba"));
		}
		catch(NoSuchPropException exc)
		{
			exc.printStackTrace();
			System.exit(-1);
		}
		//A possibility to improve this game would be to add a pattern reader here, or something like that
		//it would be in another class, probably, you would give the name of the pattern and the table and it would be filled up 
		
		initSquare(table,n,m,proba);
		
		Cell next=null;
		first=table[0][0];
		
		//I loop from the end of the table to the beginning to have a correct layout when the cells print themselves
		for(int i =n-1; i>=0;i--)
			for (int j=n-1;j>=0;j--)
			{
				table[j][i].bootLinked(getLinked(table,j,i,n),next);
				next=table[j][i];
			}
	}
	
	
	
	
	
	//here are the methods
	
	
	//this initializes cells in the square
	/*
	n, m -> same as constructor
	proba = proba to be alive
	table -> table of cells to fill
	*/
	private void initSquare(Cell[][] table,int n,int m,float proba)
	{
		int line=(n-m)/2;//the place to start/stop random living cells
		//line is good if n/m=~.5 since it just shifts the random square
		try
		{
			if ((n-m)%2==1)
				throw new InconsistentArgumentsException();
		}
		catch(InconsistentArgumentsException exc)
		{
			System.out.println("Your arguments don't have the same parity, the random square will be shifted");
			
			Miscellaneous.waiter();
			
			System.err.println("\n\nThat would have been the default behavior anyways...\nMessage to the person who will mark this assignment :\nby default, this program should just say nothing and go on, but since we are supposed to stop here, I will. Please try comenting out these lines too.");
			System.exit(0);
			
		}
		initPart(0,n,0,line,table,n);//upper part
		//there are simplifications in the initPart if line =0
		
		initPart(0        ,line   ,line, line+m,table,n);//middle left
		initPart(line     ,line+m ,line, line+m,table,n, proba);//middle
		initPart(line+m   ,n      ,line, line+m,table,n);//middle right
		
		initPart(0,n,line+m,n,table,n);//lower part
	}
	
	
	//this initializes only a rectangular part of the square, where : 
	/*
	col1=first column initialized
	col2=last one +1
	line1=first line
	line2=last one +1
	proba is still the proba to be alive
	*/
	private void initPart(int col1,int col2,int line1, int line2,Cell[][] table,int n, float proba)
	//this initializes cells maybe alive->proba = proba to be alive
	//I did not use inheritance for Random cells as asked because SideThugs might be random too
	{
		if (col1==col2 || line1==line2)
		    return;
		
		if(col1==0)
		{
			for (int j=line1;j<line2;j++)
			{
				table[0][j]=new SideThug(true,proba);
			}
			col1=1;
		}
		if(col2==n)
		{
			col2--;
			for (int j=line1;j<line2;j++)
			{
				table[col2][j]=new SideThug(false,proba);
			}
		}
		for (int i=col1;i<col2;i++)
		{
			for (int j=line1;j<line2;j++)
			{
				table[i][j]=new Cell(proba);
			}
		}
	}
	
	
	
	//this initializes dead cells, same args as the other initPart
	private void initPart(int col1,int col2,int line1, int line2,Cell[][] table, int n)
	{
		initPart(col1,col2,line1,line2,table,n,(float)0);
	}
	
	
	//this gets a board of cells that will be linked to a said cell
	/*
	col is the column of the cell
	line is the line
	n is the size
	table is the table 
	*/
	public Cell[] getLinked(Cell[][] table,int col, int line, int n)
	{
		int i;
		int j;
		boolean[][] real = new boolean[3][3];//will say which cells are real, which are not in the table
		for (i=0;i<3;i++)
			for(j=0;j<3;j++)
				real[i][j]=true;
		//gets left and right false if necessary
		if (col==0)
			for (i=0;i<3;i++)
				real[0][i]=false;
		if (col==n-1)
			for (i=0;i<3;i++)
				real[2][i]=false;
		//gets up and down false if necessary
		if (line==0)
			for (i=0;i<3;i++)
				real[i][0]= false;
		if (line==n-1)
			for (i=0;i<3;i++)
				real[i][2]=false;
		//gets middle false :
		real[1][1]=false;
		//completes linked : 
		Cell[] l=new Cell[8];
		int k=0;
		for (i=0;i<3;i++)
			for (j=0;j<3;j++)
				if (real[i][j])
				{
					l[k]=table[col+i-1][line+j-1];
					k++;
				}
		
		return Miscellaneous.removeEnd(l,k);//by removing the end I get a shorter array and don't need to check for null in Cell
	}
	
	
	
	//this updates the board and goes to the next turn
	public void update()
	{
		this.nextTurn();
		first.testCell();
	}
	
	
	//this shows the entire board on screen
	public void show()
	{
		first.showUp();
		System.out.println();
	}
	
	
	//this changes the turn for cells
	private void nextTurn()
	{
		Cell.nextTurn();
	}


}//end of class
