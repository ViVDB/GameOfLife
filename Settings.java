import java.io.*;
public abstract class Settings
/*
	this class allows to managing all settings of the game in files, without knowing said settings
	every sub-class must give an integer index to record properties, and be able to deliver/receive settings values as strings
	furthermore, a sub-class should ask this class to read the settings file at startup
*/
{
	//here are the variables
	private static final String SET_FILE="Settings.txt";
	
	
	
	
	
	//here are the methods
	
	//this method reads the options in the file and sends them back
	public static SettingPasser readSettings()
	{
		try
		{
			BufferedReader Set =new BufferedReader (new FileReader(getSetFile()));
			String buff=Set.readLine();
			int size=0;//first I get the size of the arrays I need to make, then I read the file again
			//I believe this is more efficient than change a linked list size
			while (buff!=null && buff!="") 
			{
				size++;
				Set.readLine();
				buff=Set.readLine();//every index must have a value, if this throws an exception, then there is a problem
			}
			
			Set =new BufferedReader (new FileReader( getSetFile()));
			buff=Set.readLine();
			int i=0;
			SettingPasser settings=new SettingPasser(size);
			while (buff!=null && buff!="") 
			{
				settings.indexes[i]=Integer.valueOf(buff);//--> gives index
				settings.val[i]=Set.readLine();
				
				buff=Set.readLine();
				i++;
			}
			return settings;
		}
		catch(FileNotFoundException exc)
		{
			//do nothing, no setting recorded
		}
		catch(IOException exc)
		{
			System.err.println(exc.getMessage());
			System.exit(0);
		}
		catch(Exception exc)//this is just Exception because different methods send different exception...
		{
			System.err.println("Have you accidentally modified "+getSetFile()+"? \nThis file shouldn't be touched by the user. \nPlease delete the file or undo what you did.");
			System.out.println("The game will go on but options will be default...");
			System.out.println("Please note that any human modification of "+getSetFile()+" might also make the game unstable");
			Miscellaneous.waiter();
		}
		return null;
	}
	
	
	
	
	//returns the file where settings are read/written
	public static String getSetFile()
	{
		return SET_FILE;//not a copy because it's an immutable object anyways
	}
	
	
	//records the value of settings at index index in file SET_FILE
	//this is protected, and ideally only settings subclasses (related to know which index they can use) should be in this package, along with the SettingPasser class.
	//if needed, this class can be modified to a non-static context to have several settings files.
	//(since this is a very small application it's not the case here)
	protected static void record(int index, String val)
	{
			try
			{
				
				BufferedReader set =new BufferedReader (new FileReader(SET_FILE));
				StringBuffer setBuff=new StringBuffer(128);
				String line=set.readLine();
				boolean jobDone=false;
				while (line!=null && line!="")
				{
					
					setBuff.append(line+"\n");
					if (line.equals(String.valueOf(index)))
					{
						//System.out.println("\n\nin1\n");
						set.readLine();
						setBuff.append(val);
						jobDone=true;
					}
					else
					{
						setBuff.append(set.readLine());
					}
					setBuff.append("\n");
					line=set.readLine();
				}
				set.close();
				
				if (!(new File(SET_FILE)).delete())
					throw new IOException();
				
				//System.out.println("Buffer : "+setBuff.toString());
				
				if (!jobDone)
				{
					//System.out.println("in2");
					setBuff.append(index+"\n"+val+"\n");
				}
				
				//System.out.println("Buffer 2: "+setBuff.toString());
				
				FileWriter setW=new FileWriter(SET_FILE);
				String Str=setBuff.toString();
				setW.write(Str);
				setW.close();
				
			}
			catch(FileNotFoundException exc)
			{
				try
				{
					FileWriter writer = new FileWriter(SET_FILE);
					writer.write(index+"\n");
					writer.write(val+"\n");
					writer.close();
				}
				catch(IOException e)
				{
					System.err.println(e.getMessage());
					System.exit(-1);
				}
			}
			catch(IOException exc)
			{
				System.err.println(exc.getMessage());
				System.exit(-1);
			}
		
	}
	
	
	
}
