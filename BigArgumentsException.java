//thrown if arguments too big for the program to handle nicely
public class BigArgumentsException
extends		 Exception
{
	public BigArgumentsException(int num, int limit)
	{
		super("You tried to send "+num+" as an argument, while this programs only accepts arguments of maximum "+limit +"\nPlease try GameOfLife -h");
	}
}
