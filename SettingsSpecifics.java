import java.util.Random;
import java.io.*;


/*
This class handles settings specific to this game
*/

public class SettingsSpecifics
extends Settings

{
	//here are the variables
	private static final String[] NAMES=
	{
		"M",
		"N",
		"WAITER",
		"PROBA",
		"LIMIT",
		"COLOUR ALIVE",
		"COLOUR DEAD",
		"BACK_COLOUR ALIVE",
		"BACK_COLOUR DEAD"
	};
	private static final String[] DEFAULT=
	{
		"30",
		"30",
		"150",
		"0.35",
		"80",
		Ansi.GREEN+Ansi.HIGH_INTENSITY,
		Ansi.RED+Ansi.LOW_INTENSITY,
		Ansi.BACKGROUND_BLACK,
		Ansi.BACKGROUND_BLACK
	};
	private static final String[] VALUES;
	
	
	
	
	
	
	
	
	
	
	
	//here is the constructor
	//as specified in Settings.java, it reads the settings file
	static
	{
		VALUES=new String[NAMES.length];
		for (String S : VALUES)
			S=null;
			
		// call super and ask reading file !
		
		SettingPasser setter =readSettings();
		if (setter!=null)
			for (int i=0;i<setter.val.length;i++)
			{
				try
				{
					setVal(setter.indexes[i],setter.val[i]);
				}
				catch (InconsistentArgumentsException exc)
				{
					System.err.println("There has been a problem with the arguments, some might not be applied...");
				}
			}
	}
	
	
	
	
	
	
	//here are the methods
	
	
	
	
	//this is the main function for Settings, it sorts the arguments
	//it also checks if the said arguments are valid, it's the only place where it's done
	/*
	in short the args can be 
	[blank] -> shows current settings
	[setting] [value] -> changes setting
	-h --> more info
	-r -> reset settings 
	*/
	public static void main(String[] args)
	{
		try
		{		
			if (args.length==0)
			{
				showOptions();
				return;
			}
			else
			{
				int index=0;
				boolean b=false;
				
				switch(args[0].toLowerCase())//this is separate from the rest because it can have only 1 argument
				{
					case "-h":
					case "--help":
					case "help":
						
						args[0]="-s";
						Help.show(args);
						return;
						//nothing comes back to this method later on in case of "-h"
					
					case "-r":
					case "reset":
					case "--reset":
						
						(new File(getSetFile())).delete();
						return;
						
				}
				
				while (args.length>1)//this while is to chain arguments
				{
					switch (args[0].toLowerCase())
					{
							
						case "m":
							//code for m and all integers is the same but for one value : an index--> no break, and only one code
							index=0;
							b=true;
						case "n":
							if(!b)
							{
								index=1;
								b=true;
							}
						case "waiter":
							if(!b)
							{
								index=2;
								b=true;
							}
						case "limit":
							if(!b)
							{
								index=4;
							}
							b=false;
							int m=Integer.valueOf(args[1]);
							if (m>0)
							{
								System.out.println("Note that consistency between n and m argument isn't checked here, it is your responsibility to change the m and n default if the game doesn't work.");
								recordAndSet(index,args[1]);
							}
							else
								throw new NegativeArgumentsException(m,args[0]);
							break;//everything that was integer has been taken care of
							
						
						case "proba":
							
							float p = Float.valueOf(args[1]);
							if (p>=(float) 0 && p<=(float)1)
								recordAndSet(3,args[1]);
							else
								throw new InconsistentArgumentsException();
							break;
						
						
						
						case "colour_alive":
							recCol(5,args[1]);
							break;
							
						case "colour_dead":
							recCol(6,args[1]);
							break;
							
						case "b_colour_alive":
							recCol(7,args[1]);
							break;
							
						case "b_colour_dead":
							recCol(8,args[1]);
							break;
						
						
						
						
						default:
							
							throw new NoSuchPropException();
					}
					args=Miscellaneous.removeFirst(args,2);
				}//end of while
			}//end of else
		}//end of try
		
		catch(NoSuchPropException exc)
		{
			//this is a fancy way to chain arguments to the GameOfLife and not check here any further if they are legal : in case they aren't, the user is told in GameOfLife.
			System.out.println("All valid settings registered");
			Miscellaneous.waiter();
			GameOfLife.main(args);
		}
		catch(NegativeArgumentsException exc)
		{
			System.err.println(exc.getMessage());
			System.exit(0);
		}
		catch(InconsistentArgumentsException exc)
		{
			System.err.println("Your arguments are inconsistent, check SettingsSpecifics -h or GameOfLife -h -s for more information");
			System.exit(0);
		}
		
		//chains the game of life if it hasn't been chained earlier (for example if the user sends only 1 argument)
		System.out.println("All valid settings registered");
		Miscellaneous.waiter();
		GameOfLife.main(args);
	}
	
	
	
	
	//this gets the value of the property named prop
	//throws no such property if the property asked is unknown
	public static String getProp(String prop)
	throws NoSuchPropException
	{
		switch (prop.toLowerCase())
		{
			case "m":
				return getVal(0);//obviously a break here is quite useless
			case "n":
				return getVal(1);
			case "waiter":
				return getVal(2);
			case "proba":
				return getVal(3);
			case "limit":
				return getVal(4);
			default : 
				throw new NoSuchPropException();
		}
	}
	
	//gets the value of setting at index "index"
	//is not public because in this programs you shouldn't access settings through index
	private static String getVal(int index)
	{
		if (VALUES[index]==null)
			return (DEFAULT[index]);
		else
			return (VALUES[index]);
	}
	
	//this gives the colour of the cell.
	//it's a separate function because of the Carnival mode where cells change every turn...
	//life is true if cell is alive
	public static String getColour(boolean life)
	{
		int i=6;
		if (life)
			i--;
		String[] S=new String[2];//will contain 2 colour strings
		//one for foreground, one for background
		
		S[0]=getVal(i);
		S[1]=getVal(i+2);
		
		//this changes the colour if it is Carnival
		if (S[0].equals("CARNIVAL"))//gets a random font color
		{
			Random r=new Random();
			S[0]="\u001B[3"+(r.nextInt(7)+1)+"m";
		}
		if (S[1].equals("CARNIVAL"))//gets a random back color
		{
			Random r=new Random();
			S[1]="\u001B[4"+r.nextInt(7)+"m";
		}
		
		return S[1]+S[0];
	}
	
	
	
	
	
	
	
	
	//sets the value of settings at index "index"
	//this doesn't check if the property is legal, so I made it private
	//someone should use the main to set values
	//it throws an inconsistent arguments only if somehow an empty string gets in
	private static void setVal(int index, String Val)
	throws InconsistentArgumentsException
	{
		if (Val==null || Val.equals(""))
			throw new InconsistentArgumentsException();
		VALUES[index]=Val;
	}
	
	
	
	//sets the colour property of index index to the Colour defined.
	//doesn't run check on the index !--> that's the caller's responsibility
	//throws an inconsistent argument if you ask an invisible background; that wouldn't make sense
	//throws the same exception if you don't ask something the program knows
	private static void setCol(int index,String colour)
	throws InconsistentArgumentsException
	{
		if (colour.toLowerCase().equals("carnival"))
		{
			VALUES[index]="CARNIVAL";
			return;
		}
		
		
		if (colour.toLowerCase().equals("invisible"))
		{
			if (index>6)
			{
				//background cannot be invisible
				throw new InconsistentArgumentsException();
			}
			else
			{
				VALUES[index]=Ansi.INVISIBLE_TEXT;
				return;
			}
		}
		
		String precursor="\u001B[";
		
		//tells whether it's background colour or not		
		if (index>6)
			precursor=precursor+"4";
		else
			precursor=precursor+"3";
		
		String i="0";//this will be the number determining the colour
		switch(colour.toLowerCase())
		{
			case "black":
				//i is already 0 !
				break;
			case "red":
				i="1";
				break;
			case "green":
				i="2";
				break;
			case "yellow":
				i="3";
				break;
			case "blue":
				i="4";
				break;
			case "magenta":
				i="5";
				break;
			case "cyan":
				i="6";
				break;
			case "white":
				i="7";
				break;
			default : 
				throw new InconsistentArgumentsException();
		}
		
		VALUES[index]=precursor+i+"m";
	}
	
	//records the value of settings at index index in file SET_FILE and sets it for the current game
	//private for the same reason as setVal
	private static void recordAndSet(int index, String val)
	{
		try
		{
			setVal(index,val);
		}
		catch(InconsistentArgumentsException exc)
		{
			//this should never happen :
			System.out.println("Tried to write empty argument to file");
			exc.printStackTrace();
			System.exit(0);
		}
		
		record(index, val);
		
	}
	
	
	//this function records the colour of the arguments in the settings file and sets it for the time being
	//throws inconsistent arguments from setCol
	private static void recCol(int index, String colour)
	throws InconsistentArgumentsException
	{
		//this line is to check that the argument is valid and format it
		setCol(index, colour);
		
		record(index,VALUES[index]);
	}
	
	
	//in theory, no other class needs to access this : 
	private static void showOptions()
	{
		for (int i=0;i<5;i++)
		{
			System.out.println("	"+NAMES[i]+" : "+getVal(i));
		}
		System.out.println(getColour(true)+"Living cells are like this : *"+Ansi.SANE);
		System.out.println(getColour(false)+"Dead cells are like this : -"+Ansi.SANE);
	}
	

	
	
}