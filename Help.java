import java.io.*;

//class that displays the relevant help files to the user
public class Help
{
	/*
	in short, the arguments are : 
	something : HelpOptions
	nothing : HelpGeneral
	*/
	public static void show(String[] args)
	{
		String file = "HelpOptions.txt";
		if (args.length==0)
		{
			file= "HelpGeneral.txt";
		}
		try
		{
			FileReader help =new FileReader(file);
			for(int i=help.read();i!=-1;i=help.read())
			{
				System.out.print((char)i);
			}
		}
		catch(FileNotFoundException exc)
		{
			System.err.println(exc.getMessage());
			System.err.println("Did you 'accidentally' remove that file???");
			System.exit(0);
		}
		catch(IOException exc)
		{
			System.err.println(exc.getMessage());
			System.exit(0);
		}
		System.out.println();
		return;
	}
}
