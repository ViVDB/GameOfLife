/*
	this class only contains 2 public variables to allow easy argument passing between Settings and SettingsSpecifics
*/

public class SettingPasser
{
	
	//both tabs should have the same size
	public final String[] val;
	
	public final int[] indexes;
	
	//this creates the arrays, s is the size
	public SettingPasser(int s)
	{
		val=new String[s];
		indexes=new int[s];
	}
}