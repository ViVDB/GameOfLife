//thrown when arguments seem weird
public class InconsistentArgumentsException
extends		 Exception
{
	public InconsistentArgumentsException()
	{
		super();
	}
}
