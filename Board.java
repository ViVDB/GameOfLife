import java.util.Map.Entry;
/*
This class implements a general kind of board, of any form
It is abstract since implementation depends of the shape of the board. It should be of use if I find a way to take in keyEvents in the Console or if someone implements a graphical interface
It would allow to change the activity according to keys and thus stop the game accordingly
*/
public abstract class Board
{
	//the only thing that's general for all board and can be implemented here is the activity.
	
	//here are the variables
	private boolean activity;//this is to know whether the board is still running or not
	
	
	//here is the constructor
	public Board()
	{
		activity=true;
		
		//this part should allow to check for key input...
		//this totally doesn't work
		/*
		Thread t=new Thread(new Runnable()
		{
			public void run()
			{
				while (activity)
				{
					try
					{
						Object e=new Entry();
						//crap, it's abstract, is there really no getKey() to gat keyboard value in console in java?
						//keyEvent should work in console
						//pretty sure I can't use https://github.com/kwhat/jnativehook here since it's quite complex and I already used an external file Ansi.java, so I'll stop here and not try anymore (this is myth attempt at something) 
						if (e.getKey().equals(null))
							activity=false;
						Thread.sleep(15);
					}
					catch(InterruptedException exc)
					{
						System.err.println(exc.getMessage());
					}
				}
			}
		});
		*/
	}
	
	
	//here are the methods
	
	//this method puts the board to sleep, obviously
	public void setInactive()
	{
		activity=false;
	}
	
	//this method tells if the board is sleeping
	public boolean getActivity()
	{
		return activity;
	}
	
	//this method updates the board for this turn and goes to the next turn
	public abstract void update();
	
	//this method displays the board
	public abstract void show();
	
	
}//end of class
