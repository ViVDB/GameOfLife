
import java.util.Random;
/*
This class implements a cell of a board and offers various ways to modify it
*/
public class Cell
{
	//here are the variables
	static private int TURN; //determines the turn we are in
	
	private int life;
	//life equals TURN or -TURN where the attribute changed. So you can check whether it died just now or the previous TURN, in order to call each cell only once. 
	
	private Cell[] linked;//contains all linked cells
	
	private Cell next;//this is the next cell in the table
	//it is not something like "the first element of linked" because, for example, in an O-shaped board, that wouldn't make sense 
	
	
	
	//here is the constructor
	static
	{
		//TURN definition
		TURN = 1;
	}
	//proba est la probabilité que la cellule vive à l'initialisation
	Cell(float proba)
	{
		//life definition
		Random rand=new Random();
		//proba=probabilité to be alive
		if (proba>rand.nextFloat())//cell alive
		{
			life=TURN;
			//System.out.println("Alive");
		}
		else
		{
			life=-TURN;
			//System.out.println("Dead");
		}
		
		//linked and next definition will be applied later ! otherwise, there would be some null exception because cells don't exist yet...
	}
	
	
	
	
	
	//here are the methods
	
	
	/*
	This method initializes the linked table
	arguments are quite obvious:
		linked is the linked cells and next is the next cell
	*/
	public void bootLinked(Cell[] linked,Cell next)
	{
		this.linked=linked;
		this.next=next;
	}
	
	
	
	//Shows the cell on screen
	public void showUp()
	{
		String s=System.getProperty("os.name");
		if (s.substring(0, Math.min(s.length(), 7)).toLowerCase().equals("windows"))
		{
			if (life>0)
				System.out.print("*");
			else
				System.out.print("-");			
		}
		else
		{
			if (life>0)
				System.out.print(SettingsSpecifics.getColour(true)+"*"+"\u001B[0m");//the weird characters get the color back to default (this works only on linux, I think)
			else
				System.out.print(SettingsSpecifics.getColour(false)+"-"+"\u001B[0m");			
		}
		if (!(this instanceof SideThug))
		showNext();
	}
	
	public void showNext()
	{
		if (next!=null)
			next.showUp();
	}
	
	/*
	checks if the cell is alive right now
	*/
	public boolean isAlive()
	{
		return (life>0);
	}
	
	
	//advances one turn
	//this is the cells responsability because it allows to call every cell only once per turn and because the board has no use for this number whatsoever
	public static void nextTurn()
	{
		TURN++;
		/*
		if (TURN==MAX_INT)
			throw  exception;
		that shouldn't happen, an int is a very very very big number
		*/
	}
	
	
	
	/*
	This method updates a cell for the current turn
	*/
	public void testCell()
	{
		int k=0;
		for( Cell c:linked)
			if (c.isAliveBis())
			{
				k++;
			}
		if (life>0)
		{
			if (k!=2 && k!=3)
			{
				life=-TURN;
			}
		}
		else
		{
			if (k==3)
				life=TURN;
		}
		
		if (next!=null)
		{
			next.testCell();
		}
	}
	
	
	
	//this method determines if a cell was alive up to this TURN (without caring if it already died this turn - this method is the whole reason why life is not a boolean value) 
	private boolean isAliveBis()
	{
		return ((life>0 && life<TURN) ||life==-TURN);
	}
	
	
	//tells if the cell was born this turn
	public boolean isBorn()
	{
		return life==TURN;
	}
	
	
	//tells if a cell just died...
	public boolean isDead()
	{
		return life==-TURN;
	}
}
