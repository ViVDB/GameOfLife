//exception thrown when an unknown exception is asked
public class NoSuchPropException
extends		 Exception
{
	public NoSuchPropException()
	{
		super("Oooops... \n You found a bug...\n Please report this to the developper.\nFound error : ");
	}
}
