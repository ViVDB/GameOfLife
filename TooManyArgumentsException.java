//Exception thrown if there are too many arguments
public class TooManyArgumentsException
extends		 Exception
{
	public TooManyArgumentsException(int number)
	{
		super("There were too many arguments, only the first " +number+ " have been taken into account !");
	}
}
